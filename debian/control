Source: linpsk
Maintainer: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders: Iain R. Learmonth <irl@debian.org>
Section: hamradio
Priority: optional
Build-Depends: debhelper-compat (= 12),
               qtbase5-dev,
               libasound2-dev,
               libcsnd-dev,
               libfftw3-dev,
               libhamlib-dev,
               pkg-config
Standards-Version: 4.0.0
Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/linpsk
Vcs-Git: https://salsa.debian.org/debian-hamradio-team/linpsk.git
Homepage: http://linpsk.sourceforge.net/

Package: linpsk
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: program for operating PSK31/RTTY modes with X GUI
 linpsk is a program for operating on amateur radio digital modes.
 linpsk currently supports BPSK, QPSK, and RTTY modes,
 and it provides an X user interface.  linpsk's main features are:
   - simultaneous decoding of up to four channels
   - different digital modes may be mixed
   - trigger text can be defined on each channel
   - each channel can be logged to a file
   - user-defined macros and two files for larger texts
   - spectrum and waterfall displays, both scalable in the frequency domain.
 At the Moment RTTY only supports 45 baud and 1.5 stopbits.
